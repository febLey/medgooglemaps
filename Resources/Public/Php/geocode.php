<?php

$address = $_POST['address'];
$zip = $_POST['zip'];
$city = $_POST['city'];
$country = $_POST['country'];

// Build string
$addressFinal = urlencode($address . ', ' . $zip . ' ' . $city . ', ' . $country);

$url = "http://maps.google.com/maps/api/geocode/xml?address=" . $addressFinal . "&sensor=false";
$output = file_get_contents_curl($url);

// Load XML
$xml = simplexml_load_string($output);

// Latitude/Longitude
$lat = (string) $xml->result->geometry->location->lat;
$lng = (string) $xml->result->geometry->location->lng;

$arr = array(
    "lat" => $lat,
    "lng" => $lng,
);

echo json_encode($arr);

function file_get_contents_curl($url)
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
    curl_setopt($ch, CURLOPT_URL, $url);

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}
