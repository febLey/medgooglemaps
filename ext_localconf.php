<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'MED.' . $_EXTKEY,
    'Maps',
    array(
        'Maps' => 'list',

    ),
    // non-cacheable actions
    array(
        'Maps' => '',

    )
);

if (\TYPO3\CMS\Core\Utility\GeneralUtility::compat_version('7.0')) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:medgooglemaps/Configuration/TSconfig/ContentElementWizard76.txt">');
} else {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:medgooglemaps/Configuration/TSconfig/ContentElementWizard.txt">');
}
